# Notification service
### Задача 
Необходимо разработать сервис управления рассылками API администрирования и получения статистики.
[Подробности задания](https://www.craft.me/s/n6OVYFVUpq0o6L)
### Запуск проекта(Docker)
Клонировать репозиторий и перейти в него в командной строке:
``` bash
git clone git@gitlab.com:nvkey/notification-service.git
cd notification-service
```
Создайте файл .env в папке проекта(пример: `.env.example`) со следующими уточненными ключами:
```bash
SECRET_KEY="ваш_django_секретный_ключ"

API_TOKEN="Токен api-сервиса"

EMAIL_HOST_PASSWORD="secret"
EMAIL_FROM="mail-from@yandex.ru"
EMAIL_RECIEPENT="mail-to@yandex.ru"

```
Вы можете сгенерировать `SECRET_KEY` следующим образом. Из директории `src` выполнить:

```bash
cd src
poetry install
poetry shell
python manage.py shell
from django.core.management.utils import get_random_secret_key  
get_random_secret_key()
```
Полученный ключ скопировать в .env

Запустить docker-compose:
``` bash
docker compose up -d --build
```

Проект: http://localhost/api/v1  
Flower: http://localhost:8888/

### Дополнительные задания
- 1 Тесты находятся в папке `src/tests` запуск тестов после п.3 `docker-compose exec web pytest` 
- 3 Проект запускается командой `docker compose up -d --build` при наличии сконфигурированного .env файла
- 5 Swagger API: http://localhost/docs/  
Redoc: http://localhost/redoc
- 6 Администраторский Web UI: http://localhost/admin/
- 8 Раз в сутки отправляется статистика на почту в pdf файле при наличии сконфигурированного .env файла
- 9 В сервисах `src\workers\mailing` заложена логика обновления постановки задач и обработки ошибок
- 12 Логи Celery хранятся в папке `logs`
