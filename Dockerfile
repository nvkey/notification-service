FROM python:3.11.6-slim-bullseye 

ARG YOUR_ENV
ENV YOUR_ENV=${YOUR_ENV} \
    POETRY_VERSION=1.6.1 \
    CELERY_APP=config

COPY pyproject.toml /

RUN pip install "poetry==$POETRY_VERSION"

RUN poetry $CELERY_APP virtualenvs.create false
RUN poetry install

WORKDIR /src
COPY .env src /src/
