from enum import Enum, unique
from typing import Any

from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class Tag(models.Model):
    slug = models.SlugField(
        unique=True,
        max_length=50,
        validators=[RegexValidator(regex=r"^[-a-zA-Z0-9_]+$", message="Только цифры и буквы")],
    )

    class Meta:
        ordering = ["id"]
        verbose_name = "Тэг"
        verbose_name_plural = "Тэги"

    def __str__(self) -> str:
        return str(self.slug)


class CodeOperator(models.Model):
    slug = models.SlugField(
        unique=True,
        max_length=3,
        validators=[RegexValidator(regex=r"^\d{3}$", message="Только 3 цифры")],
    )

    class Meta:
        ordering = ["id"]
        verbose_name = "Код оператора"
        verbose_name_plural = "Коды оператора"

    def __str__(self) -> str:
        return str(self.slug)


class Mailing(models.Model):
    """Рассылка."""

    date_start = models.DateTimeField(
        help_text="Дата и время запуска рассылки",
    )
    text = models.TextField(
        "Текст сообщения",
        help_text="Текст сообщения для доставки клиенту",
    )
    filter_code = models.ManyToManyField(
        CodeOperator,
        through="OperatorCodeMailing",
        related_name="mailings",
        help_text="Фильтр: код мобильного оператора",
    )
    filter_tag = models.ManyToManyField(
        Tag,
        through="TagsMailing",
        related_name="mailings",
        help_text="Фильтр: тэг",
    )
    date_stop = models.DateTimeField(
        help_text="Дата и время окончания рассылки",
    )

    class Meta:
        ordering = ["id"]
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"


class Client(models.Model):
    """Клиент."""

    phone_number = models.CharField(
        "Номер телефона",
        unique=True,
        validators=[
            RegexValidator(
                regex=r"^7\d{10}$", message="Номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)"
            )
        ],
        max_length=11,
    )
    tags = models.ManyToManyField(
        Tag,
        through="TagsClient",
        related_name="clients",
    )
    timezone = models.SmallIntegerField(
        "Часовой пояс",
        validators=[MinValueValidator(2), MaxValueValidator(12)],
        default=3,
        help_text="Часовой пояс UTC, по России от UTC+2 до UTC+12",
    )

    operator_code = models.CharField(
        "Код оператора",
        max_length=3,
        editable=False,
    )

    class Meta:
        ordering = ["id"]
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def save(self, *args: Any, **kwargs: Any) -> None:
        self.operator_code = self.phone_number[1:4]
        super().save(*args, **kwargs)


class Message(models.Model):
    """Сообщение."""

    @unique
    class Status(models.TextChoices, Enum):
        """Статус отправки."""

        ADDED = "Added", _("Добавлено в работу")
        FAILED = "Failed", _("Ошибка отправки")
        POSTED = "Posted", _("Отправлено")

    pub_date = models.DateTimeField(
        auto_now_add=True,
        help_text="Дата и время создания (отправки)",
    )
    status = models.CharField(
        "Статус отправки",
        max_length=30,
        choices=Status.choices,
    )

    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        related_name="mailing_messages",
        verbose_name="Рассылка",
    )

    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name="client_messages",
        verbose_name="Клиент",
    )

    class Meta:
        ordering = ["id"]
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"


class OperatorCodeMailing(models.Model):
    """Коды оператора рассылки."""

    code = models.ForeignKey(CodeOperator, related_name="code_mailings", on_delete=models.CASCADE)
    mailing = models.ForeignKey(Mailing, related_name="mailing_codes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Код оператора рассылки"
        verbose_name_plural = "Коды оператора рассылки"
        constraints = [models.UniqueConstraint(fields=["code", "mailing"], name="unique_code_mailing")]


class TagsMailing(models.Model):
    """Тэги рассылки."""

    tag = models.ForeignKey(Tag, related_name="tag_mailings", on_delete=models.CASCADE)
    mailing = models.ForeignKey(Mailing, related_name="mailing_tags", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Тэг рассылки"
        verbose_name_plural = "Тэги рассылки"
        constraints = [models.UniqueConstraint(fields=["tag", "mailing"], name="unique_tag_mailing")]


class TagsClient(models.Model):
    """Тэги клиента."""

    tag = models.ForeignKey(Tag, related_name="tag_clients", on_delete=models.CASCADE)
    client = models.ForeignKey(Client, related_name="client_tags", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Тэг клиента"
        verbose_name_plural = "Тэги клиента"
        constraints = [models.UniqueConstraint(fields=["tag", "client"], name="unique_tag_client")]
