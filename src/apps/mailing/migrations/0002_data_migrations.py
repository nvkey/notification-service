import json

from django.db import migrations

"""
Migrates test data from json
Format import:
[Path, App, Model]

"""

data_list = [
    ["data/tags.json", "mailing", "Tag"],
    ["data/clients.json", "mailing", "Client"],
    ["data/mailings.json", "mailing", "Mailing"],
    ["data/codes_operators.json", "mailing", "CodeOperator"],
]


def add_data(apps, schema_editor):
    for data_flow in data_list:
        path = data_flow[0]
        with open(path, encoding="utf-8") as file:
            initial_data = json.load(file)
        model = apps.get_model(data_flow[1], data_flow[2])
        initial_data_list = []
        for data in initial_data:
            new_data = model(**data)
            initial_data_list.append(new_data)
        model.objects.bulk_create(initial_data_list)


class Migration(migrations.Migration):
    dependencies = [
        ("mailing", "0001_initial"),
    ]

    operations = [migrations.RunPython(add_data)]
