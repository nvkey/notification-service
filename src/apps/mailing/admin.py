from django.contrib import admin
from django.db.models import Count, Q

from .models import Client, CodeOperator, Mailing, Message, OperatorCodeMailing, Tag, TagsClient, TagsMailing


class TagsClientAdminInLine(admin.TabularInline):
    model = TagsClient


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "phone_number",
        "timezone",
        "operator_code",
    )
    list_editable = (
        "phone_number",
        "timezone",
    )
    inlines = [TagsClientAdminInLine]


@admin.register(Tag)
class TagtAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "slug",
    )
    list_editable = ("slug",)


class TagsMailingAdminInLine(admin.TabularInline):
    model = TagsMailing


class MessageAdminInLine(admin.TabularInline):
    model = Message
    readonly_fields = [
        "pk",
        "pub_date",
        "status",
        "client",
    ]

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class OperatorCodeMailingAdminInLine(admin.TabularInline):
    model = OperatorCodeMailing


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "date_start",
        "text",
        "date_stop",
        "posted",
        "failed",
        "added",
    )

    list_editable = (
        "date_start",
        "text",
        "date_stop",
    )
    readonly_fields = (
        "posted",
        "failed",
        "added",
    )
    inlines = [TagsMailingAdminInLine, OperatorCodeMailingAdminInLine, MessageAdminInLine]

    # def queryset(self, request):
    def get_queryset(self, request):
        qs = super(MailingAdmin, self).get_queryset(request)
        return qs.annotate(
            posted=Count("mailing_messages", filter=Q(mailing_messages__status=Message.Status.POSTED)),
            failed=Count("mailing_messages", filter=Q(mailing_messages__status=Message.Status.FAILED)),
            added=Count("mailing_messages", filter=Q(mailing_messages__status=Message.Status.ADDED)),
        )

    def posted(self, inst):
        return inst.posted

    def failed(self, inst):
        return inst.failed

    def added(self, inst):
        return inst.added


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "pub_date",
        "status",
        "mailing",
        "client",
    )
    readonly_fields = (
        "pub_date",
        "status",
        "mailing",
        "client",
    )

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(CodeOperator)
class CodeOperatorAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "slug",
    )

    list_editable = ("slug",)
