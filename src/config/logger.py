import logging
import os
from logging.handlers import RotatingFileHandler

from config.settings import LOG_DIR

logger = logging.getLogger(__name__)
file_name = os.path.join(LOG_DIR, "celery_logger.log")
logger.setLevel(logging.DEBUG)
handler = RotatingFileHandler(
    file_name,
    maxBytes=50000000,
    backupCount=5,
    encoding="UTF-8",
)
logger.addHandler(handler)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s -%(funcName)s - %(message)s")
handler.setFormatter(formatter)
