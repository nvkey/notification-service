import os

from celery import Celery
from celery.schedules import crontab

from config.settings import REDIS_HOST, REDIS_PORT, TIME_ZONE

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

celery = Celery("config")

celery.config_from_object("django.conf:settings", namespace="CELERY")

celery.autodiscover_tasks()

celery.conf.update(
    broker_url="redis://" + REDIS_HOST + ":" + REDIS_PORT + "/0",
    broker_transport_options={"visibility_timeout": 3600},
    result_backend="redis://" + REDIS_HOST + ":" + REDIS_PORT + "/0",
    task_serializer="json",
    accept_content=["json"],
    result_serializer="json",
    timezone=TIME_ZONE,
    beat_schedule={
        "send-mailing": {
            "task": "workers.tasks.send_active_mailings",
            "schedule": crontab(minute="*/1"),
        },
        "send-email-stats": {
            "task": "workers.tasks.send_daytime_email",
            "schedule": crontab(minute=0, hour="*/24"),
        },
    },
)
celery.autodiscover_tasks()
