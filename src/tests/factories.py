from datetime import datetime, timedelta

import factory
import pytz
from factory.fuzzy import FuzzyChoice, FuzzyDateTime, FuzzyInteger
from faker import Faker

from apps.mailing.models import (
    Client,
    CodeOperator,
    Mailing,
    Message,
    OperatorCodeMailing,
    Tag,
    TagsClient,
    TagsMailing,
)

fake = Faker()


class MailingFactory(factory.django.DjangoModelFactory):
    """Mailing фабрика"""

    class Meta:
        model = Mailing

    date_start = FuzzyDateTime(start_dt=datetime.now(pytz.UTC) - timedelta(days=5))
    text = fake.text(max_nb_chars=50)
    date_stop = FuzzyDateTime(start_dt=datetime.now(pytz.UTC), end_dt=datetime.now(pytz.UTC) + timedelta(days=5))


class ClientFactory(factory.django.DjangoModelFactory):
    """Client фабрика"""

    class Meta:
        model = Client

    phone_number = factory.Sequence(lambda n: "79%09d" % n)
    timezone = FuzzyInteger(2, 12)


class MessageFactory(factory.django.DjangoModelFactory):
    """Message фабрика"""

    class Meta:
        model = Message

    status = FuzzyChoice(choices=Message.Status.choices)
    mailing = factory.SubFactory(MailingFactory)
    client = factory.SubFactory(ClientFactory)


class CodeOperatorFactory(factory.django.DjangoModelFactory):
    """CodeOperator фабрика"""

    class Meta:
        model = CodeOperator

    slug = factory.Sequence(lambda n: "%03d" % n)


class OperatorCodeMailingFactory(factory.django.DjangoModelFactory):
    """CodeOperatorMailing фабрика"""

    class Meta:
        model = OperatorCodeMailing

    code = factory.SubFactory(CodeOperatorFactory)
    mailing = factory.SubFactory(MailingFactory)


class TagFactory(factory.django.DjangoModelFactory):
    """Tag фабрика"""

    class Meta:
        model = Tag

    slug = factory.LazyAttribute(lambda x: fake.unique.name())


class TagsMailingFactory(factory.django.DjangoModelFactory):
    """TagsMailing фабрика"""

    class Meta:
        model = TagsMailing

    tag = factory.SubFactory(TagFactory)
    mailing = factory.SubFactory(MailingFactory)


class TagsClientsFactory(factory.django.DjangoModelFactory):
    """TagsClient фабрика"""

    class Meta:
        model = TagsClient

    tag = factory.SubFactory(TagFactory)
    client = factory.SubFactory(ClientFactory)


class ClientWith2TagsFactory(ClientFactory):
    """Client и 2 Tag фабрика"""

    class Meta:
        skip_postgeneration_save = True

    cleint_tag_1 = factory.RelatedFactory(
        TagsClientsFactory,
        factory_related_name="client",
    )
    cleint_tag_2 = factory.RelatedFactory(
        TagsClientsFactory,
        factory_related_name="client",
    )
