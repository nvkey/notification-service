from typing import Mapping, MutableMapping

from django.db.models import Model


def set_m2m_field(data: MutableMapping, field, obj: Model) -> Mapping:
    """Заменяет указанное поле на связанные объекты"""
    objs = getattr(obj, field)
    return data | {field: objs}
