from typing import Any, Dict

import pytest
from pytest_assert_utils import assert_model_attrs
from pytest_common_subject import precondition_fixture
from pytest_drf import (
    Returns200,
    Returns201,
    Returns204,
    Returns400,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesGetMethod,
    UsesListEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
    ViewSetTest,
)
from pytest_drf.util import pluralized, url_for
from pytest_lambda import lambda_fixture, static_fixture

from apps.mailing.models import Client
from tests.factories import ClientFactory, ClientWith2TagsFactory, TagFactory
from tests.utils import set_m2m_field


def express_client_obj(client_obj: Client) -> Dict[str, Any]:
    """Метод выражения класса Client"""
    return {
        "id": client_obj.pk,
        "phone_number": client_obj.phone_number,
        "operator_code": client_obj.operator_code,
        "tags": [id for id in client_obj.client_tags.values_list("tag_id", flat=True)],
        "timezone": client_obj.timezone,
    }


express_clients = pluralized(express_client_obj)


@pytest.mark.django_db(transaction=True)
class TestClientViewSet(ViewSetTest):
    list_url = lambda_fixture(lambda: url_for("clients-list"))

    detail_url = lambda_fixture(lambda client_obj: url_for("clients-detail", client_obj.pk))

    class TestList(
        UsesGetMethod,
        UsesListEndpoint,
        Returns200,
    ):
        clients_objs = lambda_fixture(
            lambda: ClientWith2TagsFactory.create_batch(size=5),
            autouse=True,
        )

        def test_it_returns_clients(self, clients_objs, results):
            expected = express_clients(sorted(clients_objs, key=lambda client: client.pk))
            actual = results
            assert expected == actual

    class TestCreate(
        UsesPostMethod,
        UsesListEndpoint,
        Returns201,
    ):
        data = lambda_fixture(
            lambda: {
                "phone_number": "79000000000",
                "tags": [tag.pk for tag in TagFactory.create_batch(3)],
                "timezone": 2,
            }
        )

        initial_client_ids = precondition_fixture(
            lambda: set(Client.objects.values_list("id", flat=True)),
            async_=False,
        )

        def test_it_creates_new_client(self, initial_client_ids, json):
            """Тест что был добавлен только один новый идентификатор."""
            expected = initial_client_ids | {json["id"]}
            actual = set(Client.objects.values_list("id", flat=True))
            assert expected == actual

        def test_it_sets_expected_attrs(self, data, json):
            """Тест значения полей, которые мы отправили POST."""
            client = Client.objects.get(pk=json["id"])
            expected = set_m2m_field(data=data, field="tags", obj=client)
            assert_model_attrs(client, expected)

        def test_it_returns_client(self, json):
            """Тест структуры ответа."""
            client = Client.objects.get(pk=json["id"])
            expected = express_client_obj(client)
            actual = json
            assert expected == actual

    class TestRetrieve(
        UsesGetMethod,
        UsesDetailEndpoint,
        Returns200,
    ):
        client_obj = lambda_fixture(lambda: ClientFactory.create())

        def test_it_returns_client(self, client_obj, json):
            expected = express_client_obj(client_obj)
            actual = json
            assert expected == actual

    class TestUpdate(
        UsesPatchMethod,
        UsesDetailEndpoint,
        Returns200,
    ):
        client_obj = lambda_fixture(lambda: ClientFactory.create())
        data = static_fixture(
            {
                "timezone": 5,
                "tags": [],
            }
        )

        def test_it_sets_expected_attrs(self, data, client_obj):
            """Тест изменений в БД"""
            client_obj.refresh_from_db()

            expected = set_m2m_field(data=data, field="tags", obj=client_obj)
            assert_model_attrs(client_obj, expected)

        def test_it_returns_client_obj(self, client_obj, json):
            """Тест структуры ответа."""
            client_obj.refresh_from_db()

            expected = express_client_obj(client_obj)
            actual = json
            assert expected == actual

    class TestDestroy(
        UsesDeleteMethod,
        UsesDetailEndpoint,
        Returns204,
    ):
        client_obj = lambda_fixture(lambda: ClientFactory.create())
        initial_client_obj_ids = precondition_fixture(
            lambda client_obj: set(Client.objects.values_list("id", flat=True)),
            async_=False,
        )

        def test_it_deletes_client_obj(self, initial_client_obj_ids, client_obj):
            expected = initial_client_obj_ids - {client_obj.pk}
            actual = set(Client.objects.values_list("id", flat=True))
            assert expected == actual

    class TestUnique(
        UsesPostMethod,
        UsesListEndpoint,
        Returns400,
    ):
        client_obj = lambda_fixture(lambda: ClientFactory.create(name="79000000000"))
        data = static_fixture(
            {"name": "79000000000"},
        )
        initial_client_obj_ids = precondition_fixture(
            lambda: set(Client.objects.values_list("id", flat=True)),
            async_=False,
        )

        def test_it_not_creates_new_client_obj(self, initial_client_obj_ids):
            """Тест записи уникального имени"""
            actual = set(Client.objects.values_list("id", flat=True))
            expected = initial_client_obj_ids
            assert expected == actual
