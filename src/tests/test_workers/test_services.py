from dataclasses import dataclass
from datetime import datetime, timedelta
from http import HTTPStatus
from unittest.mock import MagicMock, patch, sentinel

import pytest
from freezegun import freeze_time
from pytest_lambda import lambda_fixture

from apps.mailing.models import Mailing, Message
from tests.factories import (
    ClientFactory,
    CodeOperatorFactory,
    MailingFactory,
    MessageFactory,
    OperatorCodeMailingFactory,
    TagFactory,
    TagsClientsFactory,
    TagsMailingFactory,
)
from workers.services import get_active_mailings_id, get_waiting_clients_id, request_to_service


@pytest.mark.django_db(transaction=True)
class TestGetActiveMailingsId:
    """Test get_active_mailings_id."""

    @dataclass(slots=True, frozen=True)
    class Param:
        time: str
        start: str
        stop: str
        find: bool

    @pytest.fixture(
        params=[
            Param(time="2023-01-01 00:00:00+00:00", start="2023-01-01 00:00:00+00:00", stop="2024-01-01 00:00:00+00:00", find=True),
            Param(time="2024-01-01 00:00:00+00:00", start="2023-01-01 00:00:00+00:00", stop="2024-01-01 00:00:00+00:00", find=True),
            Param(time="2023-06-15 00:00:00+00:00", start="2023-01-01 00:00:00+00:00", stop="2024-01-01 00:00:00+00:00", find=True),
            Param(time="2024-01-01 00:00:01+00:00", start="2023-01-01 00:00:00+00:00", stop="2024-01-01 00:00:00+00:00", find=False),
            Param(time="2023-01-01 00:00:00+00:00", start="2023-01-01 00:00:01+00:00", stop="2024-01-01 00:00:00+00:00", find=False),
        ]
    )
    def data_fixture(self, request):
        data = request.param

        time_now = datetime.strptime(data.time, "%Y-%m-%d %H:%M:%S%z")
        time_last = time_now - timedelta(seconds=1)
        time_future = time_now + timedelta(seconds=1)

        mailings_last = set([mailing.pk for mailing in MailingFactory.create_batch(3, date_start=time_last, date_stop=time_last)])
        mailings_future = set([mailing.pk for mailing in MailingFactory.create_batch(3, date_start=time_future, date_stop=time_future)])
        mailing = set([MailingFactory.create(date_start=data.start, date_stop=data.stop).pk])

        expected = mailing if data.find else set()

        yield time_now, expected

        Mailing.objects.filter(pk__in=mailings_last | mailings_future | mailing).delete()

    def test_return_id(self, data_fixture):
        time, expected = data_fixture
        with freeze_time(time):
            actual = get_active_mailings_id()
            assert expected == actual


@pytest.mark.django_db(transaction=True)
@freeze_time("2023-01-01 00:00:00+00:00")
class TestGetWaitingClientsId:
    """Test get_waiting_clients_id"""

    def test_tz_return(self):
        timezone = 3
        mailing = MailingFactory.create(date_start=f"2023-01-01 0{timezone}:00:00+00:00")
        ClientFactory.create_batch(5, timezone=timezone - 1)
        clients_expected_now = ClientFactory.create_batch(2, timezone=timezone)
        clients_expected_past = ClientFactory.create_batch(2, timezone=timezone + 1)
        expected = set([client.id for client in clients_expected_now] + [client.id for client in clients_expected_past])
        actual = set(get_waiting_clients_id(mailing.id))
        assert expected == actual

    def test_tz_no_return(self):
        timezone = 3
        seconds = 1
        mailing = MailingFactory.create(date_start=f"2023-01-01 0{timezone}:00:0{seconds}+00:00")
        ClientFactory.create_batch(2, timezone=timezone - 1)
        ClientFactory.create_batch(2, timezone=timezone)
        expected = set()
        actual = set(get_waiting_clients_id(mailing.id))
        assert expected == actual

    def test_exclude_posted_msg_return(self):
        timezone = 2
        mailing = MailingFactory.create(date_start=f"2023-01-01 0{timezone}:00:00+00:00")
        MessageFactory.create_batch(5, mailing=mailing, client__timezone=timezone, status=Message.Status.POSTED)
        clients_expected = ClientFactory.create_batch(4, timezone=timezone)
        messages = MessageFactory.create_batch(5, mailing=mailing, client__timezone=timezone, status=Message.Status.FAILED)
        expected = set([client.id for client in clients_expected] + [message.client.id for message in messages])
        actual = set(get_waiting_clients_id(mailing.id))
        assert expected == actual

    def test_filter_code_return(self):
        timezone = 2
        ClientFactory.create(phone_number="729999999", timezone=timezone)
        code = CodeOperatorFactory.create()
        mailing_code = OperatorCodeMailingFactory.create(code=code, mailing__date_start=f"2023-01-01 0{timezone}:00:00+00:00")
        client = ClientFactory.create(phone_number=f"7{code.slug}9999999", timezone=timezone)

        expected = set([client.id])
        actual = set(get_waiting_clients_id(mailing_code.mailing.id))
        assert expected == actual

    def test_filter_code_no_return(self):
        timezone = 2
        ClientFactory.create(phone_number="729999999", timezone=timezone)
        ClientFactory.create(phone_number="739999999", timezone=timezone)
        code = CodeOperatorFactory.create(slug="999")
        mailing_code = OperatorCodeMailingFactory.create(code=code, mailing__date_start=f"2023-01-01 0{timezone}:00:00+00:00")
        expected = set([])
        actual = set(get_waiting_clients_id(mailing_code.mailing.id))
        assert expected == actual

    def test_filter_tag_return(self):
        timezone = 2
        tag = TagFactory.create()
        TagsClientsFactory.create_batch(4, client__timezone=timezone)
        tag_clients_expected = TagsClientsFactory.create_batch(4, tag=tag, client__timezone=timezone)
        tag_mailing = TagsMailingFactory.create(tag=tag, mailing__date_start=f"2023-01-01 0{timezone}:00:00+00:00")
        expected = set([tag_clients.client.id for tag_clients in tag_clients_expected])
        actual = set(get_waiting_clients_id(tag_mailing.mailing.id))
        assert expected == actual

    def test_filter_tag_no_return(self):
        timezone = 2
        tag = TagFactory.create()
        tag_2 = TagFactory.create()
        TagsClientsFactory.create_batch(4, client__timezone=timezone)
        TagsClientsFactory.create_batch(4, tag=tag, client__timezone=timezone)
        tag_mailing = TagsMailingFactory.create(tag=tag_2, mailing__date_start=f"2023-01-01 0{timezone}:00:00+00:00")
        expected = set()
        actual = set(get_waiting_clients_id(tag_mailing.mailing.id))
        assert expected == actual

    def test_filter_tag_posted_msg_return(self):
        timezone = 2
        TagsClientsFactory.create_batch(4, client__timezone=timezone)
        tag = TagFactory.create()
        tag_mailing = TagsMailingFactory.create(tag=tag, mailing__date_start=f"2023-01-01 0{timezone}:00:00+00:00")
        tag_clients_expected = TagsClientsFactory.create_batch(4, tag=tag, client__timezone=timezone)
        tag_client_expected = TagsClientsFactory.create(tag=tag, client__timezone=timezone)
        message_client_expected = MessageFactory.create(mailing=tag_mailing.mailing, client=tag_client_expected.client, status=Message.Status.FAILED)
        MessageFactory.create_batch(5, mailing=tag_mailing.mailing, client__timezone=timezone, status=Message.Status.POSTED)
        expected = set([tag_clients.client.id for tag_clients in tag_clients_expected] + [message_client_expected.client.id])
        actual = set(get_waiting_clients_id(tag_mailing.mailing.id))
        assert expected == actual


@pytest.mark.django_db(transaction=True)
@patch("workers.services.requests.post")
class TestRequestToService:
    message = lambda_fixture(lambda: MessageFactory.create(status=Message.Status.ADDED))
    requests_responce_mock = lambda_fixture(lambda: MagicMock())

    def test_it_returns_posted(self, post_mock, message, requests_responce_mock):
        requests_responce_mock.status_code = HTTPStatus.OK
        post_mock.return_value = requests_responce_mock

        actual = request_to_service(
            pk=message.pk,
            phone_number=message.client.phone_number,
            text=message.mailing.text,
        )
        expected = Message.Status.POSTED, None
        assert expected == actual

    def test_it_returns_failed(self, post_mock, message, requests_responce_mock):
        requests_responce_mock.status_code = HTTPStatus.BAD_GATEWAY
        post_mock.return_value = requests_responce_mock

        actual = request_to_service(
            pk=message.pk,
            phone_number=message.client.phone_number,
            text=message.mailing.text,
        )
        expected = Message.Status.FAILED, None
        assert expected == actual

    def test_it_returns_failed_raise(self, post_mock, message):
        sentinel.exception = ConnectionError()
        post_mock.side_effect = sentinel.exception

        actual = request_to_service(
            pk=message.pk,
            phone_number=message.client.phone_number,
            text=message.mailing.text,
        )
        expected = Message.Status.FAILED, sentinel.exception
        assert expected == actual
