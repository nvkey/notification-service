import pytest
from celery import Celery
from rest_framework.test import APIClient

from config.celery import celery


@pytest.fixture(scope="session")
def api_client() -> APIClient:
    return APIClient()


@pytest.fixture()
def celery_eager(request) -> Celery:
    celery.conf.update(task_always_eager=True)
    return celery
