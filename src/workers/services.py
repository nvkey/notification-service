from datetime import datetime
from http import HTTPStatus
from typing import Literal

import pytz
import requests
from django.core.mail import EmailMessage
from django.db.models import Q, QuerySet
from django.utils import timezone

from apps.mailing.models import Client, Mailing, Message
from config.settings import API_TOKEN, EMAIL_FROM, EMAIL_RECIEPENT

from .utils import create_pdf_with_text


def get_active_mailings_id() -> set[int]:
    """Активные рассылки"""
    time_now = timezone.now()
    mailings_id = Mailing.objects.filter(
        date_start__lte=time_now,
        date_stop__gte=time_now,
    ).values_list("id", flat=True)
    return set(mailings_id)


def get_waiting_clients_id(mailing_id: int) -> set[int]:
    """Клиенты ожидающие рассылки."""
    mailing = Mailing.objects.get(id=mailing_id)
    now = datetime.now(pytz.UTC)
    delta_hours = (mailing.date_start - now).total_seconds() / 3600

    mailing_filters = Q()
    if mailing.filter_code.exists():
        mailing_filters = mailing_filters & Q(
            operator_code__in=mailing.mailing_codes.values_list("code__slug", flat=True)
        )
    if mailing.mailing_tags.exists():
        mailing_filters = Q(tags__in=mailing.mailing_tags.values_list("tag"))
    clients = (
        Client.objects.exclude(
            client_messages__mailing=mailing,
            client_messages__status__in=[Message.Status.POSTED, Message.Status.ADDED],
        )
        .filter(Q(timezone__gte=delta_hours), mailing_filters)
        .values_list("id", flat=True)
    )
    return set(clients)


def request_to_service(
    pk: int, phone_number: str, text: str
) -> tuple[Literal[Message.Status.POSTED], None] | tuple[Literal[Message.Status.FAILED], Exception | None]:
    """Отправляет запрос на отправку сообщения через API сервис."""
    url = f"https://probe.fbrq.cloud/v1/send/{pk}"
    headers = {
        "Content-type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer {}".format(API_TOKEN),
    }
    payload = {
        "id": pk,
        "phone": int(phone_number),
        "text": text,
    }
    try:
        response = requests.post(url, json=payload, headers=headers, timeout=5)
        if response.status_code == HTTPStatus.OK:
            return Message.Status.POSTED, None
        return Message.Status.FAILED, None
    except Exception as e:
        return Message.Status.FAILED, e


def send_stats_email(mailings: QuerySet, now: datetime, period: datetime) -> None:
    subject = "Статистика"
    message = f"Статистика за прошедшие 24 часа {period} - {now}"
    from_email = EMAIL_FROM
    recipient_list = [EMAIL_RECIEPENT]
    email = EmailMessage(
        subject=subject,
        body=message,
        from_email=from_email,
        to=recipient_list,
    )
    pdf_buffer = create_pdf_with_text(mailings)
    email.attach(filename="stats.pdf", content=pdf_buffer.getvalue(), mimetype="application/pdf")
    email.send(fail_silently=True)
