import os
from io import BytesIO

import reportlab
from django.db.models.query import QuerySet
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas

from config.settings import BASE_DIR


def create_pdf_with_text(mailings: QuerySet) -> BytesIO:
    buffer = BytesIO()
    page = canvas.Canvas(buffer, pagesize=A4)
    reportlab_path = os.path.join(BASE_DIR, "static", "reportlab")
    fonts_path = os.path.join(reportlab_path, "fonts")
    reportlab.rl_config.TTFSearchPath.append(fonts_path)
    pdfmetrics.registerFont(TTFont("htc-hand", "htc-hand.ttf", "UTF-8"))

    # border A4 = 210 mm x 297 mm
    page.setStrokeColorRGB(0, 0, 0)
    page.rect(20 * mm, 35 * mm, 170 * mm, 240 * mm, fill=0)

    # list
    height = 262
    page.setFillColorRGB(0, 0, 0)
    page.setFont("htc-hand", size=32)
    page.drawCentredString(105 * mm, height * mm, "Статистика рассылок")
    page.setFont("htc-hand", size=16)
    height -= 10

    for num, mailing in enumerate(mailings):
        page.drawString(
            100,
            height * mm,
            f"{num+1} PK: {mailing.pk} Posted: {mailing.posted} Failed: {mailing.failed} Added: {mailing.added}",
        )
        height -= 8
        if height < 50:
            page.drawString(100, height * mm, "Все рассылки не поместились...")
            break

    page.drawString(20 * mm, 25 * mm, "Документ подготовлен и разработан nvkey")

    # github
    image_path = os.path.join(reportlab_path, "images", "github_white.png")
    page.drawImage(
        image_path,
        20 * mm,
        12 * mm,
        height=10 * mm,
        width=10 * mm,
        mask=(0, 0, 0, 0.3),
    )

    # link
    page.setFillColorRGB(0, 0, 204)
    page.drawString(30 * mm, 16 * mm, "https://github.com/nvkey")
    page.showPage()
    page.save()

    buffer.seek(0)
    return buffer
