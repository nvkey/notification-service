from datetime import timedelta

from celery import group
from django.db.models import Count, Q
from django.utils import timezone

from apps.mailing.models import Client, Mailing, Message
from config.celery import celery
from config.logger import logger

from .services import get_active_mailings_id, get_waiting_clients_id, request_to_service, send_stats_email


@celery.task
def send_active_mailings() -> int:
    """Отпарвка активных рассылок"""
    mailings_id = get_active_mailings_id()
    count = len(mailings_id)
    logger.debug(f"Start: {__name__}, count: {count}")
    mailings_group = group(send_mailing.s(mailing_id=id) for id in mailings_id)
    mailings_group.apply_async()
    return count


@celery.task
def send_mailing(mailing_id: int) -> int:
    """Отпарвка рассылки."""
    clients_id = get_waiting_clients_id(mailing_id)
    count = len(clients_id)
    logger.debug(f"Start: {__name__}, count: {count}")
    clients_group = group(send_message.s(mailing_id=mailing_id, client_id=id) for id in clients_id)
    clients_group.apply_async()
    return count


@celery.task
def send_message(mailing_id: int, client_id: int) -> int:
    client = Client.objects.get(id=client_id)
    mailing = Mailing.objects.get(id=mailing_id)
    time_now = timezone.now()

    if time_now > mailing.date_stop:
        logger.info(f"Mailing:{mailing.pk} - Time is over.")
        return 0
    message = Message.objects.create(
        status=Message.Status.ADDED,
        mailing=mailing,
        client=client,
    )
    status, exception = request_to_service(pk=message.pk, phone_number=client.phone_number, text=mailing.text)
    Message.objects.filter(id=message.pk).update(status=status)
    logger.warning(f"Message:{message.pk} Mailing:{mailing.pk} Client:{client.pk} {status} {repr(exception)}.")
    return message.pk


@celery.task
def send_daytime_email() -> int:
    now = timezone.now()
    period = now - timedelta(days=1)
    mailings = (
        Mailing.objects.filter(
            date_start__lte=now,
            date_stop__gte=period,
        )
        .annotate(
            posted=Count("mailing_messages", filter=Q(mailing_messages__status=Message.Status.POSTED)),
            failed=Count("mailing_messages", filter=Q(mailing_messages__status=Message.Status.FAILED)),
            added=Count("mailing_messages", filter=Q(mailing_messages__status=Message.Status.ADDED)),
        )
        .order_by("id")
    )
    if not mailings.exists():
        logger.debug("Nothing to mail")
        return 0
    send_stats_email(mailings, now, period)
    logger.debug("Send stats to email")
    return mailings.count()
