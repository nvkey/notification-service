from django.db.models import Count, Q
from rest_framework import viewsets

from apps.mailing.models import Client, CodeOperator, Mailing, Message, Tag
from .serializers import ClientSerializer, CodeOperatorSerializer, MailingSerializer, MessageSerializer, TagSerializer


class ClientViewSet(viewsets.ModelViewSet):
    """Клиенты."""

    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageViewSet(viewsets.ReadOnlyModelViewSet):
    """Сообщения."""

    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MailingViewSet(viewsets.ModelViewSet):
    """Рассылки."""

    queryset = (
        Mailing.objects.all()
        .annotate(
            msg_posted=Count("mailing_messages", filter=Q(mailing_messages__status=Message.Status.POSTED)),
            msg_failed=Count("mailing_messages", filter=Q(mailing_messages__status=Message.Status.FAILED)),
            msg_added=Count("mailing_messages", filter=Q(mailing_messages__status=Message.Status.ADDED)),
        )
        .order_by("id")
    )
    serializer_class = MailingSerializer


class TagViewSet(viewsets.ModelViewSet):
    """Тэги."""

    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class CodeOperatorViewSet(viewsets.ModelViewSet):
    """Коды операторов."""

    queryset = CodeOperator.objects.all()
    serializer_class = CodeOperatorSerializer
