from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, CodeOperatorViewSet, MailingViewSet, MessageViewSet, TagViewSet

router = DefaultRouter()

router.register("clients", ClientViewSet, basename="clients")
router.register("messages", MessageViewSet, basename="messages")
router.register("mailings", MailingViewSet, basename="mailings")
router.register("tags", TagViewSet)
router.register("codes", CodeOperatorViewSet)
urlpatterns = [
    path("v1/", include(router.urls)),
]
