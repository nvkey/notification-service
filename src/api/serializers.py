from typing import MutableMapping

from django.shortcuts import get_object_or_404
from rest_framework import serializers

from apps.mailing.models import Client, CodeOperator, Mailing, Message, Tag

from .services import create_filters_mailing, create_tags_client


class ClientSerializer(serializers.ModelSerializer):
    timezone = serializers.IntegerField(max_value=12, min_value=2)
    tags = serializers.PrimaryKeyRelatedField(many=True, queryset=Tag.objects.all())

    def create(self, validated_data: MutableMapping) -> Client:
        tags = validated_data.pop("tags")
        client = Client.objects.create(**validated_data)
        create_tags_client(client, tags)
        return client

    def update(self, instance: Client, validated_data: dict) -> Client:
        client = get_object_or_404(Client, pk=instance.pk)
        tags = validated_data.pop("tags")
        instance.tags.clear()
        create_tags_client(client, tags)
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance

    def validate_tags(self, value: list) -> list:
        if len(set(value)) != len(value):
            raise serializers.ValidationError("Теги повторяются")
        return value

    class Meta:
        model = Client
        fields = ("id", "phone_number", "operator_code", "tags", "timezone")


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ("id", "pub_date", "status", "mailing", "client")


class MailingSerializer(serializers.ModelSerializer):
    filter_code = serializers.PrimaryKeyRelatedField(many=True, queryset=CodeOperator.objects.all())
    filter_tag = serializers.PrimaryKeyRelatedField(many=True, queryset=Tag.objects.all())
    msg_posted = serializers.IntegerField(read_only=True)
    msg_failed = serializers.IntegerField(read_only=True)
    msg_added = serializers.IntegerField(read_only=True)

    def create(self, validated_data: dict) -> Mailing:
        codes = validated_data.pop("filter_code")
        tags = validated_data.pop("filter_tag")
        mailing = Mailing.objects.create(**validated_data)
        create_filters_mailing(mailing=mailing, codes=codes, tags=tags)
        return mailing

    def update(self, instance: Mailing, validated_data: dict) -> Mailing:
        mailing = get_object_or_404(Mailing, pk=instance.pk)
        codes = validated_data.pop("filter_code")
        tags = validated_data.pop("filter_tag")
        instance.filter_code.clear()
        instance.filter_tag.clear()
        create_filters_mailing(mailing=mailing, codes=codes, tags=tags)
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance

    def validate_filter_tag(self, value: list) -> list:
        if len(set(value)) != len(value):
            raise serializers.ValidationError("Теги повторяются")
        return value

    class Meta:
        model = Mailing
        fields = (
            "id",
            "date_start",
            "text",
            "date_stop",
            "filter_code",
            "filter_tag",
            "msg_posted",
            "msg_failed",
            "msg_added",
        )


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ("id", "slug")


class CodeOperatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = CodeOperator
        fields = ("id", "slug")
