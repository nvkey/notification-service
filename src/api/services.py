from typing import Iterable

from django.shortcuts import get_object_or_404

from apps.mailing.models import Client, CodeOperator, Mailing, OperatorCodeMailing, Tag, TagsClient, TagsMailing


def create_tags_client(client: Client, tags: Iterable[Tag]) -> None:
    for tag in tags:
        current_tag = get_object_or_404(Tag, id=tag.pk)
        TagsClient.objects.get_or_create(client=client, tag=current_tag)


def create_filters_mailing(mailing: Mailing, codes: Iterable, tags: Iterable) -> None:
    for code in codes:
        current_code = get_object_or_404(CodeOperator, id=code.pk)
        OperatorCodeMailing.objects.get_or_create(mailing=mailing, code=current_code)
    for tag in tags:
        current_tag = get_object_or_404(Tag, id=tag.pk)
        TagsMailing.objects.get_or_create(mailing=mailing, tag=current_tag)
